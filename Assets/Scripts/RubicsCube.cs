﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TwoPhaseSolver;

[System.Serializable]
public class RotationCommand
{
    public RotationCommand(RotatorLocation location, bool inverted, float speed = 0.15f)
    {
        this.rotationLocation = location;
        this.inverted = inverted;
        this.speed = speed;
    }

    public RotatorLocation rotationLocation;
    public bool inverted = false;
    public float speed = 0f;
}

public class RubicsCube : MonoBehaviour {

    public Button scrambleButton;
    public Button resetButton;
    public Button toggleSolverButton;

    public int turnCount = 0;

    public List<RotatorObject> Rotators = new List<RotatorObject>();

    public bool executeCommands = false;

    public List<RotationCommand> rotationQueue = new List<RotationCommand>();
    public List<RotationCommand> commandHistory = new List<RotationCommand>();

    public bool autoSolve = false;

    private RotationCommand currentCommand = null;

    bool newCommand = false;

    float sleepTimer = 0f;
    float sleepTime = 0f;

    private GameObject[] rotateButtons;
    
    T RandomEnumValue<T>(int key)
    {
        var v = Enum.GetValues(typeof(T));
        return (T)v.GetValue(key);
    }

    void LoadInitialState()
    {
        SceneManager.LoadScene("cubes");
    }
    

    public void RotateOnClick(RotateButton rBtn)
    {
        RotationCommand cmd = new RotationCommand(rBtn.buttonRotateLocation, rBtn.inverted);
        rotationQueue.Add(cmd);
    }


    Vector3 pivot;
    public float speed = 45f;

    void Start()
    {

        rotateButtons = GameObject.FindGameObjectsWithTag("RotateBtn");
       

        for (int i = 0; i < rotateButtons.Length; i++)
        {
            Button _btn = rotateButtons[i].GetComponent<Button>();
            RotateButton rotateBtnScript = rotateButtons[i].GetComponent<RotateButton>();

            _btn.onClick.AddListener(rotateBtnScript.RotatePlane);
        }

        scrambleButton.onClick.AddListener(() => {
            Scramble();
        });
        
        resetButton.onClick.AddListener(() => {
            LoadInitialState();
        });

        var bounds = new Bounds();
        bounds.center = transform.position;

        var transforms = transform.root.GetComponentsInChildren<Transform>();
        foreach (Transform trans in transforms)
        {
            bounds.Encapsulate(trans.position);
        }

        pivot = bounds.center;

        toggleSolverButton.onClick.AddListener(() => {
            autoSolve = !autoSolve;
        });
    }

    private string CMDSTRING = "U D2 F L' R2";

    public void SolveAutomatically()
    {
        if (commandHistory.Count == 0)
        {
            autoSolve = false;
            return;
        }

        var cmdString = CmdToString();

        Cube c = new Cube();
        Move pattern = new Move(cmdString);

        c = pattern.apply(c);

        Search.patternSolve(c, pattern, 22, printInfo: true);

        pattern = Search.patternSolve(c, pattern, 22, printInfo: false);

        rotationQueue = StringToCmd(pattern.ToString());
        
        commandHistory.Clear();
        autoSolve = false;
    }

    public void Scramble()
    {
        int i = 20;
        //UnityEngine.Random random = new UnityEngine.Random();

        for (int j = 0; j < i; j++)
        {
            bool inverted = Convert.ToBoolean(UnityEngine.Random.Range(0, 2));

            RotatorLocation randomRotator = RandomEnumValue<RotatorLocation>(UnityEngine.Random.Range(0, 5));
            
            RotationCommand cmd = new RotationCommand(randomRotator, inverted, 0f);
            rotationQueue.Add(cmd);
            //rotationQueue[rotationQueue.Count - 1];
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.root.RotateAround(pivot, Vector3.down, Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.root.RotateAround(pivot, Vector3.up, Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.root.RotateAround(pivot, Vector3.left, Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.root.RotateAround(pivot, Vector3.right, Time.deltaTime * speed);
        }
        
        if (autoSolve)
        {
            SolveAutomatically();
        }
    }

    // Update is called once per frame
    void FixedUpdate () {



        UpdateSleepTimer();
        TransitionTime.CalculateTransitions();

        if (!executeCommands)
        {
            return;
        }

        if (IsSleep())
        {
            return;
        }

        if (currentCommand == null)
        {
            if (rotationQueue.Count == 0)
            {
                return;
            }

            newCommand = true;
            turnCount++;
            currentCommand = rotationQueue[0];
            rotationQueue.Remove(rotationQueue[0]);
            commandHistory.Add(currentCommand);

            EnableRotatorCollisions(currentCommand.rotationLocation);
            return;
        }



        string sideName = currentCommand.rotationLocation.ToString();
        bool inverted = currentCommand.inverted;
        string inv = currentCommand.rotationLocation.ToString();
        if (inverted){
            inv = "inverted";
        }
        else if (!inverted)
        {
            inv = String.Empty;
        }

        if (!HasCubeCollisions())
        {
            return;
        }

        if (newCommand)
        {
            AssignChildrenToRotator();
            
            TransitionTime.CreateLinearTransition(sideName, currentCommand.speed, inverted ? -90 : 90);

            newCommand = false;
        }

        MaybeRotatePlane(currentCommand.rotationLocation);

        if (!TransitionTime.IsTransitioning(sideName))
        {
            ClearChildrenFromRotators();
            DisableRotatorCollisions(currentCommand.rotationLocation);
            currentCommand = null;

            Sleep(0.01f);
        }
    }

    private string CmdToString()
    {
        string Commands = "";
        for(int i = 0; i < commandHistory.Count; i++)
        {
            if (commandHistory[i].rotationLocation == RotatorLocation.up)
            {
                Commands = Commands + "U";
            }

            if (commandHistory[i].rotationLocation == RotatorLocation.front)
            {
                Commands = Commands + "F";
            }

            if (commandHistory[i].rotationLocation == RotatorLocation.right)
            {
                Commands = Commands + "R";
            }

            if (commandHistory[i].rotationLocation == RotatorLocation.down)
            {
                Commands = Commands + "D";
            }

            if (commandHistory[i].rotationLocation == RotatorLocation.back)
            {
                Commands = Commands + "B";
            }

            if (commandHistory[i].rotationLocation == RotatorLocation.left)
            {
                Commands = Commands + "L";
            }

            if (commandHistory[i].inverted == true)
            {
                Commands = Commands + "'";
            }

            if (i < commandHistory.Count - 1)
                Commands = Commands + " ";
        }
        return Commands;
    }

    private List<RotationCommand> StringToCmd(string cmnd)
    {
        List<RotationCommand> Commands = new List<RotationCommand>();
        string[] stringArray = cmnd.Split(' ');
        for(int i = 0; i < stringArray.Length; i++)
        {
            if (stringArray[i] == "U'")
            {
                Commands.Add(new RotationCommand(RotatorLocation.up, true));
            }

            if (stringArray[i] == "F'")
            {
                Commands.Add(new RotationCommand(RotatorLocation.front, true));
            }

            if (stringArray[i] == "R'")
            {
                Commands.Add(new RotationCommand(RotatorLocation.right, true));
            }

            if (stringArray[i] == "D'")
            {
                Commands.Add(new RotationCommand(RotatorLocation.down, true));
            }

            if (stringArray[i] == "B'")
            {
                Commands.Add(new RotationCommand(RotatorLocation.back, true));
            }

            if (stringArray[i] == "L'")
            {
                Commands.Add(new RotationCommand(RotatorLocation.left, true));
            }

            if (stringArray[i] == "U2")
            {
                Commands.Add(new RotationCommand(RotatorLocation.up, false));
                Commands.Add(new RotationCommand(RotatorLocation.up, false));
            }

            if (stringArray[i] == "F2")
            {
                Commands.Add(new RotationCommand(RotatorLocation.front, false));
                Commands.Add(new RotationCommand(RotatorLocation.front, false));
            }

            if (stringArray[i] == "R2")
            {
                Commands.Add(new RotationCommand(RotatorLocation.right, false));
                Commands.Add(new RotationCommand(RotatorLocation.right, false));
            }

            if (stringArray[i] == "D2")
            {
                Commands.Add(new RotationCommand(RotatorLocation.down, false));
                Commands.Add(new RotationCommand(RotatorLocation.down, false));
            }

            if (stringArray[i] == "B2")
            {
                Commands.Add(new RotationCommand(RotatorLocation.back, false));
                Commands.Add(new RotationCommand(RotatorLocation.back, false));
            }

            if (stringArray[i] == "L2")
            {
                Commands.Add(new RotationCommand(RotatorLocation.left, false));
                Commands.Add(new RotationCommand(RotatorLocation.left, false));
            }

            if (stringArray[i] == "U")
            {
                Commands.Add(new RotationCommand(RotatorLocation.up, false));
            }

            if (stringArray[i] == "F")
            {
                Commands.Add(new RotationCommand(RotatorLocation.front, false));
            }

            if (stringArray[i] == "R")
            {
                Commands.Add(new RotationCommand(RotatorLocation.right, false));
            }

            if (stringArray[i] == "D")
            {
                Commands.Add(new RotationCommand(RotatorLocation.down, false));
            }

            if (stringArray[i] == "B")
            {
                Commands.Add(new RotationCommand(RotatorLocation.back, false));
            }

            if (stringArray[i] == "L")
            {
                Commands.Add(new RotationCommand(RotatorLocation.left, false));
            }
        }

        return Commands;
    }

    bool HasCubeCollisions()
    {
        int i = 0;
        for (i = 0; i < Rotators.Count; i++)
        {

            if (Rotators[i].rotatorLocation != currentCommand.rotationLocation)
            {
                continue;
            }

            Rotator rotator = Rotators[i].gameObject.GetComponent<Rotator>();

            var colliders = rotator.getCollidingBoxes();

            if (colliders.Count != 9)
            {
                return true;
            }

            return true;
        }

        return false;
    }

    void Sleep(float time = 0f)
    {
        sleepTime = time;
        sleepTimer = 0f;
    }

    bool IsSleep()
    {
        if (sleepTime > sleepTimer)
        {
            return true;
        }

        return false;
    }

    void UpdateSleepTimer()
    {
        sleepTimer += Time.fixedDeltaTime;
    }

    void EnableRotatorCollisions(RotatorLocation side)
    {
        int i= 0;

        for (i = 0; i < Rotators.Count; i++)
        {
            Rotator rotator = Rotators[i].gameObject.GetComponent<Rotator>();

            if (Rotators[i].rotatorLocation == side)
            {
                rotator.EnableCollisions();
            }
        }
    }

    void DisableRotatorCollisions(RotatorLocation side)
    {
        int i = 0;

        for (i = 0; i < Rotators.Count; i++)
        {
            Rotator rotator = Rotators[i].gameObject.GetComponent<Rotator>();

            if (Rotators[i].rotatorLocation == side)
            {
                rotator.DisableCollisions();
            }
        }
    }


    void RotateSide(RotatorLocation side, bool inverted = false)
    {
        var rotationCommand = new RotationCommand(side, inverted);
        rotationQueue.Add(rotationCommand);
    }

    Vector3 GetSideVector(RotatorLocation side)
    {
        switch (side)
        {
            case RotatorLocation.up:
                return Vector3.up;

            case RotatorLocation.down:
                return Vector3.up;

            case RotatorLocation.left:
                return Vector3.left;

            case RotatorLocation.right:
                return Vector3.left;

            case RotatorLocation.front:
                return Vector3.forward;

            case RotatorLocation.back:
                return Vector3.forward;

            default:
                return Vector3.forward;
        }
        
    }

    void MaybeRotatePlane(RotatorLocation rLocation)
    {
        int i = 0;

        for (i = 0; i < Rotators.Count; i++)
        {
            if (Rotators[i].rotatorLocation == rLocation)
            {
                Rotators[i].gameObject.transform.Rotate(Vector3.right * TransitionTime.GetTransition(rLocation.ToString()));
            }

        }
    }


    void ClearChildrenFromRotators()
    {
        int i, j = 0;

        for (i = 0; i < Rotators.Count; i++)
        {
            Rotator rotator = Rotators[i].gameObject.GetComponent<Rotator>();

            var colliders = rotator.getCollidingBoxes();

            for (j = 0; j < colliders.Count; j++)
            {
                if (colliders[j].CompareTag("Cube"))
                {
                    colliders[j].transform.parent = transform;
                }
            }
           
        }
    }

    void AssignChildrenToRotator()
    {
        int i, j = 0;

        for (i = 0; i < Rotators.Count; i++)
        {

            if (Rotators[i].rotatorLocation != currentCommand.rotationLocation)
            {
                continue;
            }

            Rotator rotator = Rotators[i].gameObject.GetComponent<Rotator>();

            var colliders = rotator.getCollidingBoxes();
            
            for (j = 0; j < colliders.Count; j++)
            {
                if (colliders[j].CompareTag("Cube"))
                {
                    colliders[j].transform.parent = Rotators[i].gameObject.transform;
                }
            }
        }
    }
}

