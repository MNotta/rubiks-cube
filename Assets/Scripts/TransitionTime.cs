﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    class TransitionTime
    {
        private static Dictionary<string, TransitionObject> Transitions = new Dictionary<string, TransitionObject>();

        public static void CreateLinearTransition(string key, float duration, float to, float from = 0)
        {
            if (!Transitions.ContainsKey(key))
            {
                TransitionObject transition = new TransitionObject(duration, to, from);
                Transitions.Add(key, transition);
            }
        }

        public static bool IsTransitioning(string key)
        {
            TransitionObject transObject = null;

            Transitions.TryGetValue(key, out transObject);

            if (transObject != null)
            {
                return true;
            }
            
            return false;
        }

        public static float GetTransition(string key)
        {
            TransitionObject transObject = null;

            Transitions.TryGetValue(key, out transObject);

            if (transObject == null)
            {
                return 0f;
            }

            if (transObject.HasFinished())
            {
                var finalProgress = transObject.GetFinalProgress();
                
                Transitions.Remove(key);
                return finalProgress;
            }

            return transObject.GetProgress();
        }

        public static void CalculateTransitions()
        {
            foreach (KeyValuePair<string, TransitionObject> transition in Transitions)
            {
                transition.Value.ApplyDeltaTime();
            }
        }
    }
}
