﻿using UnityEngine;

[System.Serializable]
public enum RotatorLocation
{
    up, front, down, left, back, right
}

[System.Serializable]
public class RotatorObject
{
    public GameObject gameObject = null;
    
    public RotatorLocation rotatorLocation;

}


