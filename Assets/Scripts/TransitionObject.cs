﻿using UnityEngine;

namespace Assets.Scripts
{
    class TransitionObject
    {
        private float totalDuration = 0f;
        private float currentDuration = 0f;

        private float to = 0;
        private float from = 0;

        private float lastPoint = 0;

        public TransitionObject(float duration, float to, float from)
        {
            this.totalDuration = duration;
            this.to = to;
            this.from = from;

            ApplyDeltaTime();
        }

        public void ApplyDeltaTime()
        {
            currentDuration += Time.fixedDeltaTime;
        }

        public float GetProgress()
        {
            var currentPointAdvanced = to * (currentDuration / totalDuration) - lastPoint;

            lastPoint += currentPointAdvanced;

            return currentPointAdvanced;
        }
 
        public float GetFinalProgress()
        {
            return to - lastPoint;
        }

        public bool HasFinished()
        {
            return currentDuration >= totalDuration;
        }
    }
}
