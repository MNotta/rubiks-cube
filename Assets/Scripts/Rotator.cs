﻿using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    
    public List<GameObject> collidingBoxes = new List<GameObject>();

    private bool collisionsEnabled = false;

    public void Update()
    { 

        //var rigidbody = gameObject.GetComponent<Rigidbody>();

        //if (rigidbody.IsSleeping())
        //{
        //    rigidbody.WakeUp();
        //}
    }

    public List<GameObject> getCollidingBoxes()
    {
        return collidingBoxes;
    }
    
    public void EnableCollisions()
    {
        collisionsEnabled = true;
    }

    public void DisableCollisions()
    {
        collidingBoxes = new List<GameObject>();
        collisionsEnabled = false;
    }

    void OnCollisionStay(Collision col)
    {
        if (!collisionsEnabled)
        {
            return;
        }

        if (!collidingBoxes.Contains(col.gameObject))
        {
            collidingBoxes.Add(col.gameObject);
        }
    }
}
