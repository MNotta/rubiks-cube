﻿using UnityEngine;

public class RotateButton : MonoBehaviour
{
    public RotatorLocation buttonRotateLocation;
    public bool inverted = false;
    

    public RubicsCube mainScript;

    public void Start()
    {
        mainScript = GameObject.FindGameObjectWithTag("Player").GetComponent<RubicsCube>();
    }

    public void RotatePlane()
    {
        mainScript.RotateOnClick(this);
        return;
    }
}
